#! /bin/sh

if [[ $1 == 00* ]] || [[ $1 == 99* ]];
then
	cat $1 | sed 's/\\chapter{/\\chapterno{/'
else
	cat $1
fi
