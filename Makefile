MD_PARTS= ${wildcard ./parts/*.md}
TEX_PARTS= ${notdir ${MD_PARTS:.md=.tex}}

all: project.pdf

project.pdf: output.pdf
	cp $< $@

output.pdf: parts.tex output.tex
	latexmk -pdf -pdflatex="pdflatex -interactive=nonstopmode -shell-escape -halt-on-error" -use-make output.tex
#	pdflatex -shell-escape -halt-on-error output.tex

output.tex: template.tex metadata.json
	python ./generate_main_tex.py

template.tex: src/template.tex
	cp $< $@

parts.tex: inputs.tmp
	cat $< | sort | awk '{printf "\\input{%s}\n", $$1}' > parts.tex

inputs.tmp: ${TEX_PARTS}

%.tex: %.tmp.tex
	./customize.sh $< > $@
	echo "$@" >> inputs.tmp

%.tmp.tex: parts/%.md
	~/.cabal/bin/pandoc $< --chapters -o $@

clean: proper
	rm -f *.pdf 

proper:
	latexmk -CA
	rm -f *.tex *.glo *.ist *.tmp
