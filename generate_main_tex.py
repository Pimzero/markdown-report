import json
import pystache

metadata = json.load(open("./metadata.json", 'r'))
template = open("./template.tex", 'r').read()
rendered = pystache.render(template, metadata)
open("./output.tex", 'w').write(rendered)
